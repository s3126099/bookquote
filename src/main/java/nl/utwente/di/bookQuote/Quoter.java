package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String, Double> bookTable = new HashMap<>();

    public Quoter() {
        bookTable.put("1", 10.0);
        bookTable.put("2", 45.0);
        bookTable.put("3", 20.0);
        bookTable.put("4", 35.0);
        bookTable.put("5", 50.0);
    }


    public double getBookPrice(String isbn) {
        Double price;
        if ((price = bookTable.get(isbn)) == null) return 0.0;
        return price;
    }
}
